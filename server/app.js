var express = require('express'),
    cors = require('cors'),
    path = require('path'),
    fs = require('fs'),
    port = 3000,
    app = express();

app.use(cors());

app.use('/', express.static(__dirname + '/../client/build'));

app.get('/dealer', function (req, res) {
  setTimeout(function () {
    res.sendFile(path.normalize(__dirname + '/resources/dealers.json'));
  }, 1000);
});

app.get('/dealer/:id', function (req, res) {
  var dealersPath = path.normalize(__dirname + '/resources/dealers.json'),
      dealerId = parseInt(req.params.id, 10),
      searchedDealer;

  fs.readFile(dealersPath, 'utf8', function (err, data) {
    if (err) throw err;

    var dealers = JSON.parse(data);

    dealers.forEach(function (dealer) {
      if (dealer.id === dealerId) {
        searchedDealer = dealer;
      }
    });

    res.json(searchedDealer);
  });
});

app.get('/dealer/:id/comments', function (req, res) {
  var commentsPath = path.normalize(__dirname + '/resources/comments.json'),
      dealerId = parseInt(req.params.id, 10),
      returnedComments = [];

  fs.readFile(commentsPath, 'utf8', function (err, data) {
    if (err) throw err;

    var comments = JSON.parse(data); 

    comments.forEach(function (comment) {
      if (comment.dealerId === dealerId) {
        returnedComments.push(comment);
      }
    });

    setTimeout(function () {
      res.json(returnedComments);
    }, 3000);
  });
});

app.listen(port, function () {
  console.log('Server running at port: ', port);
});