var config = require('./config'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    jshint = require('gulp-jshint'),
    inject = require('gulp-inject'),
    concat = require('gulp-concat'),
    copy = require('gulp-contrib-copy'),
    uglify = require('gulp-uglify'),
    htmlmin = require('gulp-htmlmin'),
    clearCSS = require('gulp-clean-css'),
    gulpFilter = require('gulp-filter'),
    ngAnnotate = require('gulp-ng-annotate'),
    bowerFiles = require('main-bower-files');

gulp.task('handle:html', function () {
  return gulp.src(config.path.src + '/app/**/*.html')
    .pipe(gulp.dest(config.path.build + '/app'));
});

gulp.task('handle:images', function () {
  return gulp.src(config.path.src + '/public/images/**')
    .pipe(gulp.dest(config.path.build + '/public/images'));
});

gulp.task('handle:fonts', function () {
  return gulp.src(bowerFiles())
    .pipe(gulpFilter(['**/*.eot', '**/*.svg', '**/*.ttf', '**/*.woff']))
    .pipe(gulp.dest(config.path.build + '/public/fonts'));
});

gulp.task('minify:html', ['inject'], function() {
  return gulp.src(config.path.build + '/index.html')
    .pipe(htmlmin({ 
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest(config.path.build));
});

gulp.task('handle:js', function () {
  return gulp.src([
      config.path.src + '/app/app.module.js',
      config.path.src + '/app/**/*.js'
    ])
    .pipe(ngAnnotate())
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(config.path.build + '/public/js'));
});

gulp.task('handle:js:libs', function () {
  return gulp.src(bowerFiles())
    .pipe(gulpFilter('**/*.js', { restore: true }))
    .pipe(concat('libs.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(config.path.build + '/public/js'));
});

gulp.task('handle:css:libs', function () {
  return gulp.src(bowerFiles())
    .pipe(gulpFilter('**/*.css', { restore: true }))
    .pipe(concat('libs.min.css'))
    .pipe(clearCSS())
    .pipe(gulp.dest(config.path.build + '/public/css'));
});

gulp.task('scss', function () {
  return gulp.src([
      config.path.src + '/app/components/_core/_core.scss',
      config.path.src + '/app/**/*.scss'
    ])
    .pipe(concat('main.min.scss'))
    .pipe(sass())
    .pipe(gulp.dest(config.path.build + '/public/css'));
});

gulp.task('lint', function () {
  return gulp.src(config.path.build + '/app/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('inject', ['handle:images', 'handle:js', 'handle:js:libs', 'handle:css:libs'], function () {
  var sources = gulp.src([
      config.path.build + '/public/js/*.js',
      config.path.build + '/public/css/*.css'
    ]);

  return gulp.src(config.path.src + '/index.html')
    .pipe(inject(sources, {
      relative: true,
      transform: function (filepath, file, i, length) {
        var filepathSplit = filepath.split('.'),
            filetype = filepathSplit[filepathSplit.length - 1],
            newFilePath = filepath.replace('../build/', '');

        if (filetype === 'css') {
          return '<link rel="stylesheet" href="' + newFilePath + '" />';
        } else if (filetype === 'js') {
          return '<script type="text/javascript" src="' + newFilePath + '"></script>';
        }
      }
    }))
    .pipe(gulp.dest(config.path.build));
});

gulp.task('build', [
    'handle:html',
    'handle:fonts',
    'handle:images',
    'handle:js',
    'handle:js:libs',
    'handle:css:libs',
    'scss',
    'lint',
    'inject',
    'minify:html'
  ], function () {
    console.log('Build finished: ', new Date());
  });