angular.module('EAN').directive('footer', function () {
  'use strict';

  return {
    templateUrl: 'app/shared/footer/footer.html'
  };
});