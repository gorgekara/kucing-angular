angular.module('EAN').directive('dealer', function () {
  'use strict';

  return {
    scope: {
      dealerName: '@',
      dealerRating: '@',
      dealerId: '@',
      dealerTagline: '@',
      dealerPage: '='
    },
    templateUrl: 'app/shared/dealer/dealer.html',
    link: function (scope, element, attrs) {
      $(".my-rating").starRating({
        totalStars: 5,
        starShape: 'rounded',
        starSize: 40,
        emptyColor: 'lightgray',
        hoverColor: 'salmon',
        activeColor: 'crimson',
        useGradient: false
      });
    }
  };
});