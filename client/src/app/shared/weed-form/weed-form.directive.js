angular.module('EAN').directive('weedForm', function () {
  'use strict';

  return {
    scope: {
      weedDealer: '='
    },
    templateUrl: 'app/shared/weed-form/weed-form.html',
    link: function (scope, element, attrs) {
      scope.weed = {};
    }
  };
});