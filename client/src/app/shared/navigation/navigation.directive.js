angular.module('EAN').directive('navigation', function () {
  'use strict';

  return {
    templateUrl: 'app/shared/navigation/navigation.html',
    link: function (scope, element, attrs) {
      
      scope.logout = function () {
        alert('log out user!');
      };

    }
  };
});