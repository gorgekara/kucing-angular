angular.module('EAN').directive('commentInputForm', function () {
  'use strict';

  return {
    scope: {
      commentList: '=',
      dealerId: '@'
    },
    templateUrl: 'app/shared/comment-input-form/comment-input-form.html',
    link: function (scope, element, attrs) {
      scope.comment = {};
      
      scope.createComment = function () {
        scope.commentList.push({
          id: scope.commentList.length + 1,
          dealerId: scope.dealerId,
          name: "Current user",
          comment: scope.comment.content,
          date_created: new Date()
        });

        scope.comment.content = '';
      };

    }
  };
});