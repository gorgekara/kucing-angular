EAN.directive('starRating', function () {
  'use strict';

  return {
    scope: {
      dealerRating: '@'
    },
    templateUrl: 'app/shared/star-rating/star-rating.html',
    link: function (scope, element, attrs) {
      scope.$watch('dealerRating', function (newVal) {
        if (newVal) {
          $(element).starRating({
            totalStars: 10,
            initialRating: newVal,
            starShape: 'rounded',
            starSize: 40,
            emptyColor: 'lightgray',
            hoverColor: 'salmon',
            activeColor: 'crimson',
            useGradient: false,
            readOnly: true
          });
        }
      });

      element.on('$destroy', function () {
        $(element).starRating('unload');
      });
    }
  };
});