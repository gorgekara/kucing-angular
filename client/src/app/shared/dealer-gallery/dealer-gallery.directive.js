angular.module('EAN').directive('dealerGallery', function () {
  'use strict';

  return {
    replace: true,
    scope: {
      gallery: '='
    },
    templateUrl: 'app/shared/dealer-gallery/dealer-gallery.html',
    link: function (scope, element, attrs) {

      scope.$watch('gallery', function (newVal, oldVal) {
        if (newVal) {
          scope.activeImage = scope.gallery[0];
        }
      });

      scope.selectImage = function (image) {
        scope.activeImage = image;
      };

    }
  };
});