angular.module('EAN').directive('comment', function () {
  'use strict';

  return {
    scope: {
      data: '=',
    },
    templateUrl: 'app/shared/comment/comment.html'
  };
});