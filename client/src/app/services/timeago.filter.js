angular.module('EAN').filter('timeago', function () {
  'use strict';
  
  return function (input) {
    return moment(input).fromNow();
  };
});