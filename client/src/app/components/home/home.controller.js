angular.module('EAN').controller('HomeController', function ($scope, ApiService, localStorageService) {
  'ngInject';
  'use strict';

  $scope.dealerOrder = localStorageService.cookie.get('filter') || '-rating';

  $scope.orderDealers = function (item) {
    $scope.dealerOrder = item;
    localStorageService.cookie.set('filter', item);
  };
  
  ApiService.get({
    url: '/dealer',
    success: function (response) {
      $scope.dealers = response;
    }
  });
});