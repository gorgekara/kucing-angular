angular.module('EAN').controller('HeaderController', function ($rootScope) {
  'ngInject';
  'use strict';

  $rootScope.head = {
    title: 'EAN - Express Angular Node',
    description: 'Test project for learning purposes only!'
  };
});