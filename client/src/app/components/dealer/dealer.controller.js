angular.module('EAN').controller('DealerController', function ($rootScope, $scope, $state, ApiService) {
  'ngInject';
  'use strict';

  var dealerId = $state.params.id;
  
  ApiService.get({
    url: '/dealer/' + dealerId,
    success: function (response) {
      if (!response) {
        $state.go('app.404');
      }

      $scope.dealer = response;

      $rootScope.head.title = 'EAN - Dealer: ' + $scope.dealer.name;
    }
  });

  ApiService.get({
    url: '/dealer/' + dealerId + '/comments',
    success: function (response) {
      $scope.comments = response;
    }
  });
});