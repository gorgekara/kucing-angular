angular.module('EAN').controller('CoreController', function ($rootScope) {
  'ngInject';
  'use strict';
  
  $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
    if (toState.data && toState.data.title) {
      $rootScope.head.title = toState.data.title;
    }
  });
});