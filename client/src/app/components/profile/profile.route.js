angular.module('EAN').config(function ($stateProvider) {
  'ngInject';
  'use strict';

  $stateProvider
    .state('app.profile', {
      url: '/profile',
      controller: 'ProfileController',
      templateUrl: 'app/components/profile/profile.html',
      data: {
        title: 'EAN - Profile'
      }
    });
});