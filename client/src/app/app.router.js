angular.module('EAN').config(function ($stateProvider, $urlRouterProvider, localStorageServiceProvider) {
  'ngInject';
  'use strict';

  localStorageServiceProvider
    .setPrefix('EAN')
    .setStorageType('sessionStorage');

  $urlRouterProvider.otherwise('/404');
  $urlRouterProvider.when('', '/');

  $stateProvider
    .state('app', {
      url: '',
      abstract: true,
      controller: 'CoreController',
      templateUrl: 'app/components/_core/_core.layout.html'
    })
    .state('app.home', {
      url: '/',
      controller: 'HomeController',
      templateUrl: 'app/components/home/home.html',
      data: {
        title: 'EAN - Express Angular Node'
      }
    })
    .state('app.dealer', {
      url: '/dealer/{id:int}',
      controller: 'DealerController',
      templateUrl: 'app/components/dealer/dealer.html'
    })
    .state('app.contact', {
      url: '/contact-us',
      templateUrl: 'app/components/contact-us/contact-us.html',
      data: {
        title: 'EAN - Contact Us'
      }
    })
    .state('app.about', {
      url: '/about-us',
      templateUrl: 'app/components/about-us/about-us.html',
      data: {
        title: 'EAN - About Us'
      }
    })
    .state('app.terms', {
      url: '/terms-and-conditions',
      templateUrl: 'app/components/terms-and-conditions/terms-and-conditions.html',
      data: {
        title: 'EAN - Terms and conditions'
      }
    })
    .state('app.404', {
      url: '/404',
      templateUrl: 'app/components/404/404.html',
      data: {
        title: 'EAN - Error 404'
      }
    });
});